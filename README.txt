
Module: Webform Feedback
Author: Protitude <http://drupal.org/user/79388>


Description
===========
Adds a pop-up lightbox like feedback form.
Inspired by http://www.feedbackify.com

Requirements
============

* Webform
* Libraries
* Jquery Update

Installation
============
* Copy the 'webform_feedback' module directory in to your Drupal
sites/all/modules directory as usual.
* Enable module (requires webform module).
* Set the "Webform Feedback Block - Ajax Call" to show up
on your site either through context or the default block
page. The location is up to you. I usually set it in the
footer area.

Optional - Installation
=======================
* Grab the inert polyfill from github: https://github.com/WICG/inert
* Take the file from 'inert/dist/inert.min.js' and place it in your libraries
  folder 'sites/all/libraries/inert/inert.min.js'
* Under "Non-selectable CSS classes or ID's" add the class or id (including # or
  . preceeding) of the areas outside of the feedback form. This is to add 'inert'
  to each tag that shouldn't be selectable when the feedback form is active. In
  other words if you tab through your website you can make it so only the form
  can be tabbed through along with the address bar, thereby making it much easier
  to not get lost if a user has a screen reader.

Optional - for current webforms
===============================
* Create a webform that you want to use as a
pop-up.
* Go to webform_feedback settings
(admin/config/content/webform-feedback).
	* Select your webform and hit save.
  * Set up a context or set the region on the default block page.
